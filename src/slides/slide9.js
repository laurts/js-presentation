import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Closure</Heading>
    <Layout>
      <Fill>
        <CodeView label="What will this output">
          {`
function upate(propertyName){
  return function(value){
    this.state[propertyName] = value;
  }
}

<input type="text" onChange={this.update("username")}/>
 `}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
