import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Event loop</Heading>
    <Layout>
      <Fill>
        <CodeView label="What will this output">
          {`(function(a) {
  console.log(a);
  console.log(1);
  setTimeout(function(){console.log(2)}, 1000);
  setTimeout(function(){console.log(3)}, 0);
  console.log(4);
 })(“IIFE”);
 `}
        </CodeView>
        <CodeView label="What will this output" appear>
          {`for (var i = 0; i < 5; i++) {
  setTimeout(function() { console.log(i); }, i * 1000 );
 }
 `}
        </CodeView>
        <CodeView label="What will this output" appear>
          {`
for (var i = 0; i < 5; i++) {
 (function(x){
   setTimeout(function() { console.log(x); }, x * 1000 );
 })(i)
}`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
