import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Constructor prototype</Heading>
    <Layout>
      <Fill>
        <CodeView label="prototype ">
          {`
function User(name) {
  this.name = name;
}
//special propety name on a Function and a Function only
User.prototype.sayHi = function(){
  return this.name;
}

`}
        </CodeView>
        <CodeView label="Separating instance from class" appear>
          {`let Handler = {
  nameLen: function(){
    return this.name.length
  }
};

let userInstance = {name:"Juhan"}
userInstance.__proto__= Handler
userInstance.nameLen() // 5;

`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
