import React from 'react';
import { Slide, Heading, Layout, Fill, SpeakerNotes } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Function binding</Heading>
    <Layout>
      <Fill>
        <CodeView label="Losing this">
          {`
let user = {
  firstName: "John",
  sayHi() {
    alert(\`Hello, ${this.firstName}!\`);
  }
};

setTimeout(user.sayHi, 1000); // Hello, undefined!
`}
        </CodeView>
        <CodeView label="wrapper" appear>{`
setTimeout(function() {
  user.sayHi(); // Hello, John!
}, 1000);`}
        </CodeView>
        <CodeView label="bind" appear>{`
let sayHi = user.sayHi.bind(user);//we take the method user.sayHi and bind it to user
`}</CodeView>
        <CodeView label="What is the issue here" appear>{`
var user = {
  name:'Juhan',
  getName:function(){
    return this.name;
  }
}

var userFn = user.getName;

console.log(userFn())
console.log(user.getName())

`}</CodeView>
      </Fill>
    </Layout>
  </Slide>
);
