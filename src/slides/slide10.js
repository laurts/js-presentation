import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Rest and spread</Heading>
    <Layout>
      <Fill>
        <CodeView label="Syntax">
          {`
// For function calls
myFunction(...iterableObj);

//For array literals or strings:
[...iterableObj, '4', 'five', 6];

//For object literals
let objClone = { ...obj };
 `}
        </CodeView>
        <CodeView label="Function spread Examples" appear>
          {`
function myFunction(x, y, z) { }
var args = [0, 1, 2];
myFunction(...args);

`}
        </CodeView>
        <CodeView label="Array Examples" appear>
          {`
var arr = [1, 2, 3];
var arr1 = [3, 4, 5];
//copy array
var arr3 = [...arr]; // like arr.slice()

//merge array
var arr4 = [...arr, ...arr2]; // arr4 is now [0, 1, 2, 3, 4, 5]
    arr4 = [...arr2, ...arr]; // arr4 is now [3, 4, 5, 0, 1, 2]

`}
        </CodeView>
        <CodeView label="Dynamic result">
          {`
async function(){
  var [result, err] = async http.get('www.google.com')
  if(err){
    //ups
  } 
}
`}
        </CodeView>
        <CodeView label="Spread in object literals" appear>
          {`
var obj1 = { foo: 'bar', x: 42 };
var obj2 = { foo: 'baz', y: 13 };

var clonedObj = { ...obj1 }; //{ foo: "bar", x: 42 }
var mergedObj = { ...obj1, ...obj2 }; // Object { foo: "baz", x: 42, y: 13 }


`}
        </CodeView>
        <CodeView label="Rest" appear>
          {`
var obj1 = { foo: 'bar', x: 42, y:13, z:5 };
var {x, y, ...other} = obj;


`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
