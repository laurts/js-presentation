import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Data types</Heading>
    <Layout>
      <Fill>
        <CodeView label="We can declare multipel variables in one line">
          {`let user = 'John', age = 25, message = 'Hello world';`}
        </CodeView>
        <CodeView label="Use let instead var" appear>
          {`let message = "hello world";
message = 123456;

var message = "hello";`}
        </CodeView>
        <CodeView label="Reseved names" appear>
          {`let let = 5; // can't name a variable "let", error!
let return = 5; // also can't name it "return", error!`}
        </CodeView>

        <CodeView label="Constants" appear>
          {`const message="hello"
message="world!"; // error, can't reassign the constant!

const obj = {
  key:'world';
}

obj.key='world wide';  // can change object contstant,
// object is not constant but, pointer to the object
obj={key:'data'};      // error, can't reassign the constant!
          `}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
