import React from 'react';
import { Slide, Heading, Layout, Fill, Text } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Objects</Heading>
    <Text>In JavaScript, an ‘object’ is effectively just a key–value structure.</Text> 
<Text>It is what in other languages is called a hash, a map or a dictionary</Text>

    <Layout>
      <Fill>
        <CodeView label="An empty object">
          {`let user = new Object(); // "object constructor" syntax
let user = {   // "object literal" syntax
  name: "Juhan",
  age: 30,
  "likes birds": true  // multiword property name must be quoted
};
user.alias="J";
user["likes birds"] = false;
// delete
delete user["likes birds"];
`}
        </CodeView>
        <CodeView label="Reserver words are allowed within object" appear>
          {`let obj = {
  for: 1,
  let: 2,
  return: 3
}
alert( obj.for + obj.let + obj.return );
//except __proto__
`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
