import React from 'react';
import { Slide, Heading, Layout, Fill, Fit } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Common JS modules</Heading>
    <Layout>
      <Fit>
        <CodeView label="CJS Exports">
          {`
//
const A = require("mod");

//named exports
exports.a = "a";
exports.b = "b";

// a default export

module.exports = function(){
  //do something
}
 `}
        </CodeView>
      </Fit>
      <Fit>
        <CodeView label="Ecma script module ESM" appear>
          {`
import A from 'mod';

//named exports
export let a = "a"
export let b = "b"

// a default export

export default function(){
  //do something
}

`}
        </CodeView>
      </Fit>
    </Layout>
  </Slide>
);
