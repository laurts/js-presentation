import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Arrays</Heading>
    <Layout>
      <Fill>
        <CodeView label="Declaring">
          {`var a = new Array();
var a = [1, 2, 3];
a[10] = 99; //Will this crash?
console.log(a[6]); //what will this output?
`}
        </CodeView>
        <CodeView label="Mix of values" appear>
          {`let arr = [ 'Apple', { name: 'John' }, true, function() { alert('hello'); } ];
arr[3](); //alert
arr.age = 25; //Will this crash?
`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
