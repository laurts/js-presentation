import React from 'react';
import { Slide, Heading, Layout, Fill, Fit } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Common JS modules</Heading>
    <Layout>
      <Fit>
        <CodeView label="CJS">
          {`
//
const A = require("mod");

// named imports
const {a, b} = require("mod");

// a default import

const fun = require("mod").default;

`}
        </CodeView>
      </Fit>
      <Fit>
        <CodeView label="ESM" appear>
          {`
import A from "mod";

//named imports
import {a, b} from "mod";

// a default import

import fun from "mod";

// aliased import

import * as func from "mod";
console.log(func.a) //a
import {a as a1, b as b1} from "mod";

`}
        </CodeView>
      </Fit>
      <Fill>
        <CodeView label="Pro TIP!" appear>
          {`
//esm module loader is IIFE
(function(exports, require, module, __filename, __dirname){
  export const foo = "d";

})
`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
