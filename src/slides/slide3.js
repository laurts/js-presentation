import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Cloning and merging</Heading>
    <Layout>
      <Fill>
        <CodeView>
          {`let user = {
  name: "Juhan",
  age: 30
};

let clone = {}; // the new empty object

// let's copy all user properties into it
for (let key in user) {
  clone[key] = user[key];
}`}
        </CodeView>
        <CodeView label="Object.assign" appear>
          {`let user = { name: "Juhan",
  sizes: {
    height: 182,
    width: 50
  }
};
// overwrite name, add isAdmin
Object.assign(user, { name: "Pete", isAdmin: true });
let clone = Object.assign({}, user);

`}
        </CodeView>
        <CodeView label="Question" appear>{`
user.sizes.width++;       // change a property from one place
console.log(user.sizes.width)  //? what is the output
`}</CodeView>
      </Fill>
    </Layout>
  </Slide>
);
