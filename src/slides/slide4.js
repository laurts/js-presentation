import React from 'react';
import { Slide, Heading, Layout, Fill } from 'spectacle';
import { CodeView } from '../components';

export default (
  <Slide transition={['fade']} style={{ 'max-height': 'fit-content' }}>
    <Heading size={1}>Constructor function</Heading>
    <Layout>
      <Fill>
        <CodeView>
          {`
function User(name) {
  this.name = name;
  this.isAdmin = false;
  this.sayHi = function() {
    alert( "My name is: " + this.name );
  };
  return; //trick, finishes the execution, returns this
}

console.log(new User("Jack").name);
let jill = new User("Jill");
console.log(jill.sayHi())

let user = new User; // <-- no parentheses if it has no arguments
`}
        </CodeView>
      </Fill>
    </Layout>
  </Slide>
);
