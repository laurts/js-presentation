// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  BlockQuote,
  Markdown,
  CodePane,
  Cite,
  Deck,
  Heading,
  ListItem,
  List,
  Quote,
  Fill,
  Fit,
  Layout,
  Slide,
  Text
} from 'spectacle';

//import createTheme from 'spectacle/lib/themes/default';
import CodeSlide from 'spectacle-code-slide';
import createTheme from 'spectacle-theme-nova';

var CodeMirror = require('react-codemirror');

require('codemirror/mode/javascript/javascript');
// Import theme

// Require CSS
require('normalize.css');
require('spectacle/lib/themes/default/index.css');
require('codemirror/lib/codemirror.css');
require('./assets/style.css');
const theme = createTheme({
//  primary: '#f2f2f2'
});


export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
        progress="bar"
      >
        <Slide transition={['zoom']} bgImage="https://cdn-images-1.medium.com/max/1400/1*raWO3dhM4jMjf9VY-kZzNg.png">
          <Heading size={1} fit caps lineHeight={1} textColor="white">
            Javascripti keele nüansid praktikutele
          </Heading>
        </Slide>
        {require('./slides/slide1').default}
        {require('./slides/slide2').default}
        {require('./slides/slide3').default}
        {require('./slides/slide4').default}
        {require('./slides/slide5').default}
        {require('./slides/slide6').default}
        {require('./slides/slide7').default}
        {require('./slides/slide8').default}
        {require('./slides/slide9').default}
        {require('./slides/slide10').default}
        <Slide transition={['fade']}>
          <Heading size={1}>Tänud</Heading>
        </Slide>
      </Deck>
    );
  }
}
