import React from 'react';
import { Appear } from 'spectacle';
var CodeMirror = require('react-codemirror');

export const CodeView = ({ children, label, appear }) => {
  let comp = (
    <div>
      {label && (
        <h6 style={{ 'margin-bottom': '.5rem', 'margin-top': '1rem' }}>
          {label}
        </h6>
      )}

      <CodeMirror
        value={children}
        options={{
          lineNumbers: true,
          highlightTheme: 'bespin',
          mode: 'javascript'
        }}
      />
    </div>
  );
  return !appear ? comp : <Appear>{comp}</Appear>;
};
